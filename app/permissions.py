from rest_framework import permissions

class IsOwnerOrReadOnly(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True

        return obj.user == request.user

class IsSuperuser(permissions.BasePermission):
	def has_object_permission(self, request, view, obj):
		return request.user.is_superuser

class IsPhotosessionLocationOwner(permissions.BasePermission):
	def has_object_permission(self, request, view, obj):
		return obj.device.location.owner == request.user

class IsMediaLocationOwner(permissions.BasePermission):
	def has_object_permission(self, request, view, obj):
		return obj.photosession.device.location.owner == request.user