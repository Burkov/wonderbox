from celery import shared_task
import time, os

import app.models
from django.conf import settings
from django.core.files import File
from django.utils import timezone

from processing.combine_vdnh import combine
from processing.send_sms import send_sms


@shared_task
def wait(t, photosession_pk):
    # Don't pass models as arguments to tasks, pass model pk's. 
    # Or your data will be obsolete by the time task runs.
    time.sleep(t)

    # You can access the database from here, just don't create circular
    # task calls. E.g. if you modify photosession in response to post_save
    # signal on photosession, your task will call itself in an endless loop.
    
    result = None

    photosession = app.models.Photosession.objects.get(pk=photosession_pk)
    media = app.models.Media.objects.filter(photosession=photosession)

    if not photosession.scheduled:
        result = "Photosession is already processed"

    elif len(media) == 0:
        result = "Missing input media"

    else:
        result = {}

        photosession.scheduled = False
        photosession.save()

        result_path = combine(media[0].file.path, wait.request.id)
        result_path = os.path.join(settings.BASE_DIR, 'app', 'processing', result_path)

        base, ext = os.path.splitext(result_path)
        ts = timezone.now().strftime("%H-%M-%S") + ext

        final_img = app.models.Media(photosession=photosession, metadata='{}', is_public=True)
        final_img.file.save(ts, File(open(result_path)))
        final_img.save()

        result['Image'] = result_path

        if photosession.sms_status != "delivered":
            r = send_sms(photosession.user.telephone)
            if r['success']:
                photosession.sms_status = "delivered"
            result['SMS'] = r

        photosession.save()

    return result
