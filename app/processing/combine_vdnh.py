import os, errno
from PIL import Image
from django.conf import settings

def affirm_path(path):
    try:
        os.makedirs(path)
    except OSError as exception:
        if exception.errno != errno.EEXIST:
            raise

def combine(input_path, task_id=None):
    out_dir = os.path.join(settings.BASE_DIR, "task_data", "out")
    overlay_path = os.path.join(settings.BASE_DIR, "task_data", "vdnh_overlay_1.png")
    affirm_path(out_dir)

    img = Image.open(input_path, 'r')
    img_w, img_h = img.size

    overlay = Image.open(overlay_path, 'r')
    overlay_w, overlay_h = overlay.size
    # offset = ((img_w - overlay_w) / 2, (img_h - overlay_h) / 2)
    img.paste(overlay, (0, 0), overlay)

    base = os.path.basename(input_path)
    base, ext = os.path.splitext(base)

    name = '%s_final%s' % (base, ext)
    if task_id:
        name = '%s_%s%s' % (base, task_id, ext)
    out_path = os.path.join(out_dir, name)
    img.save(out_path)
    return out_path
