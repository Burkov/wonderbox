from smsc_api import SMSC

def send_sms(phone):
    link = 'http://photopost.me/' + phone    
    message = "%s" % link
    smsc = SMSC()
    r = smsc.send_sms(phone, message)

    result = {
        'id'        : r[0],
        'success'   : int(r[1]) > 0,
    }

    if result['success']:
        try:
            result['price'] = r[2]
            result['balance'] = r[3]
        except IndexError:
            pass
    else:
        result['error'] = r[1]

    return result


