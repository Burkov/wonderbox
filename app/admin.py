from django.contrib import admin
from django.contrib.admin.widgets import AdminFileWidget
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from django.utils.safestring import mark_safe
from django.forms import ModelForm

from app.models import User, Location, Campaign, Device, MediacoverTemplate, MediacoverTemplateFile, UserSiteTemplate, UserSiteTemplateFile, Media, Photosession 
from app.forms import CustomUserChangeForm, CustomUserCreationForm

class CustomUserAdmin(UserAdmin):
    fieldsets = (
        (None, {'fields': ('telephone', 'location', 'password')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')})
    )

    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('telephone', 'location', 'password1', 'password2', 'is_superuser', 'is_staff')}
        ),
    )

    form = CustomUserChangeForm
    add_form = CustomUserCreationForm
    list_display = ('telephone', 'location', 'is_superuser', 'is_staff', 'is_active', 'date_joined')
    search_fields = ('telephone',)
    ordering = ('telephone',)

admin.site.register(User, CustomUserAdmin)
admin.site.register(Location)
admin.site.register(Campaign)
admin.site.register(Device)
admin.site.register(MediacoverTemplate)
admin.site.register(MediacoverTemplateFile)
admin.site.register(UserSiteTemplate)
admin.site.register(UserSiteTemplateFile)

# MediaAdmin code mostly stolen from:
# http://stackoverflow.com/questions/1385094/django-admin-and-showing-thumbnail-images
# http://stackoverflow.com/questions/13174440/django-admin-how-to-display-thumbnail-instead-of-path-to-file
# http://blog.glaucocustodio.com/2014/04/08/display-thumbnails-in-django-admin-with-sorl-thumbnail/
# it required image_tag method in Media model

class AdminMediaWidget(AdminFileWidget):
    def render(self, name, value, attrs=None):
        output = []
        if value and getattr(value, 'url', None):
            output.append(u'<img src="%s" width=100/>' % (value.url))
        output.append(super(AdminFileWidget, self).render(name, value, attrs))
        return mark_safe(u''.join(output))

class MediaForm(ModelForm):
    class Meta:
        model = Media
        fields = ['file', 'photosession', 'metadata']
        widgets = {
            'file': AdminMediaWidget
        }

class MediaInline(admin.TabularInline):
    model = Media
    form = MediaForm

# Semi-editable admin for staff users stolen from:
# http://reinout.vanrees.org/weblog/2011/09/30/django-admin-filtering.html
# http://stackoverflow.com/questions/6310983/django-admin-specific-user-admin-content

class MediaAdmin(admin.ModelAdmin):
    def get_queryset(self, request):
        queryset = super(MediaAdmin, self).get_queryset(request)
        if request.user.is_superuser and request.user.is_active:
            return queryset
        else:
            return queryset.filter(photosession__device__location__owner=request.user)

    def change_view(self, request, object_id, form_url='', extra_context=None):
        if not self.queryset(request).filter(id=object_id).exists():
            return HttpResponseRedirect(reverse('admin:myapp_mymodel_changelist'))

        return super(MediaAdmin, self).change_view(request, object_id, form_url, extra_context)

    def delete_view(self, request, object_id, extra_context=None):
        if not self.queryset(request).filter(id=object_id).exists():
            return HttpResponseRedirect(reverse('admin:myapp_mymodel_changelist'))

        return super(MediaAdmin, self).delete_view(request, object_id, extra_context)

    def history_view(self, request, object_id, extra_context=None):
        if not self.queryset(request).filter(id=object_id).exists():
            return HttpResponseRedirect(reverse('admin:myapp_mymodel_changelist'))

        return super(MediaAdmin, self).history_view(request, object_id, extra_context)

    list_display = ('image_tag', 'photosession', 'metadata')

admin.site.register(Media, MediaAdmin)

class PhotosessionAdmin(admin.ModelAdmin):
    def get_queryset(self, request):
        queryset = super(PhotosessionAdmin, self).get_queryset(request)
        if request.user.is_superuser and request.user.is_active:
            return queryset
        else:
            return queryset.filter(device__location__owner=request.user)

    def change_view(self, request, object_id, form_url='', extra_context=None):
        if not self.queryset(request).filter(id=object_id).exists():
            return HttpResponseRedirect(reverse('admin:myapp_mymodel_changelist'))

        return super(PhotosessionAdmin, self).change_view(request, object_id, form_url, extra_context)

    def delete_view(self, request, object_id, extra_context=None):
        if not self.queryset(request).filter(id=object_id).exists():
            return HttpResponseRedirect(reverse('admin:myapp_mymodel_changelist'))

        return super(PhotosessionAdmin, self).delete_view(request, object_id, extra_context)

    def history_view(self, request, object_id, extra_context=None):
        if not self.queryset(request).filter(id=object_id).exists():
            return HttpResponseRedirect(reverse('admin:myapp_mymodel_changelist'))

        return super(PhotosessionAdmin, self).history_view(request, object_id, extra_context)

    list_filter = ('datetime', 'device', 'user')
    ordering = ('-datetime',)
    inlines = [
        MediaInline,
    ]

admin.site.register(Photosession, PhotosessionAdmin)

from djcelery.models import TaskMeta
class TaskMetaAdmin(admin.ModelAdmin):
    readonly_fields = ('result', )

admin.site.register(TaskMeta, TaskMetaAdmin)

