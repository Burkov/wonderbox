from django.conf.urls import patterns, include, url
from django.contrib.auth import login, authenticate, logout

from rest_framework.urlpatterns import format_suffix_patterns

from app.views import *

api_photosessions = ApiPhotosessionViewSet.as_view({
    'get': 'list',
    'post': 'create'
})

api_photosession = ApiPhotosessionViewSet.as_view({
    'get': 'detail',
    'put': 'update',
    'delete': 'destroy'
})


urlpatterns = patterns('',
    url(r'^$',
        index_view,
        {},
        name='index'),
    url(r'^(?P<telephone>[0-9]+)/$',
        gallery_view,
        {},
        name='gallery'),

    url(r'^dashboard/$',
        dashboard_view,
        {},
        name='dashboard'),

    url(r'^api/$',
        api_root_view,
        {},
        name='api_root'),

    url(r'^api/user/$',
        ApiUsersView.as_view(),
        {},
        name='api_users'),
    url(r'^api/user/(?P<telephone>[0-9]+)/$',
        ApiUserView.as_view(),
        {},
        name='api_user'),

    url(r'^api/location/$',
        ApiLocationsView.as_view(),
        {},
        name="api_locations"),
    url(r'^api/location/(?P<pk>[0-9]+)/$',
        ApiLocationView.as_view(),
        {},
        name="api_location"),

    url(r'^api/campaign/$',
        ApiCampaignsView.as_view(),
        {},
        name="api_campaigns"),
    url(r'^api/campaign/(?P<pk>[0-9]+)/$',
        ApiCampaignView.as_view(),
        {},
        name="api_campaign"),

    url(r'^api/device/$',
        ApiDevicesView.as_view(),
        {},
        name='api_devices'),
    url(r'^api/device/(?P<pk>[0-9]+)/$',
        ApiDeviceView.as_view(),
        {},
        name='api_device'),

    url(r'^api/mediacover_template/$',
        ApiMediacoverTemplatesView.as_view(),
        {},
        name='api_mediacover_templates'),
    url(r'api/mediacover_template/(?P<pk>[0-9]+)/$',
        ApiMediacoverTemplateView.as_view(),
        {},
        name='api_mediacover_template'),

    url(r'^api/mediacover_template_file/$',
        ApiMediacoverTemplateFilesView.as_view(),
        {},
        name='api_mediacover_template_files'),
    url(r'api/mediacover_template_file/(?P<pk>[0-9]+)/$',
        ApiMediacoverTemplateFileView.as_view(),
        {},
        name='api_mediacover_template_file'),

    url(r'^api/user_site_template/$',
        ApiUserSiteTemplatesView.as_view(),
        {},
        name='api_user_site_templates'),
    url(r'^api/user_site_template/(?P<pk>[0-9]+)/$',
        ApiUserSiteTemplateView.as_view(),
        {},
        name='api_user_site_template'),

    url(r'^api/user_site_template_file/$',
        ApiUserSiteTemplateFilesView.as_view(),
        {},
        name='api_user_site_template_files'),
    url(r'^api/user_site_template_file/(?P<pk>[0-9]+)/$',
        ApiUserSiteTemplateFileView.as_view(),
        {},
        name='api_user_site_template_file'),

    url(r'^api/media/$',
        ApiMediasView.as_view(),
        {},
        name='api_medias'),
    url(r'^api/media/(?P<pk>[0-9]+)/$',
        ApiMediaView.as_view(),
        {},
        name='api_media'),

    url(r'^api/photosession/$',
        api_photosessions,
        {},
        name='api_photosessions'),
    url(r'^api/photosession/(?P<pk>[0-9]+)/$',
        api_photosession,
        {},
        name='api_photosession'),    

)

urlpatterns = format_suffix_patterns(urlpatterns, allowed=['json', 'html'])
