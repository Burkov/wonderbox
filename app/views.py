from collections import OrderedDict

from django.http import Http404
from django.shortcuts import render
from django.template.response import TemplateResponse

from django.contrib.sites.shortcuts import get_current_site
from django.contrib.admin.views.decorators import staff_member_required

from django.views.decorators.csrf import csrf_protect, csrf_exempt
from django.views.decorators.debug import sensitive_post_parameters

from django.core.urlresolvers import reverse
from django.core.exceptions import ObjectDoesNotExist
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from rest_framework.parsers import JSONParser
from rest_framework.renderers import JSONRenderer
from rest_framework.decorators import api_view
from rest_framework.views import APIView
from rest_framework import viewsets
from rest_framework import status
from rest_framework.response import Response
from rest_framework import permissions

from models import User, Location, Campaign, Device, MediacoverTemplate, MediacoverTemplateFile, UserSiteTemplate, UserSiteTemplateFile, Media, Photosession 
from serializers import UserSerializer, LocationSerializer, CampaignSerializer, DeviceSerializer, MediacoverTemplateSerializer, MediacoverTemplateFileSerializer, UserSiteTemplateSerializer, UserSiteTemplateFileSerializer, MediaSerializer, PhotosessionGetPutSerializer, PhotosessionPostSerializer
from permissions import IsOwnerOrReadOnly, IsSuperuser, IsPhotosessionLocationOwner, IsMediaLocationOwner

@csrf_protect
@sensitive_post_parameters('password1', 'password2', 'password')
def index_view(request):
    context = {'user': request.user,
               'site': get_current_site(request),
               'site_name': get_current_site(request).name,
              }
    return TemplateResponse(request, 'index.html', context)

@csrf_protect
def gallery_view(request, telephone):
    try:
        owner = User.objects.get(telephone=telephone)
    except ObjectDoesNotExist:
        raise Http404

    photos_list = Media.objects.filter(photosession__user=owner, public=True).order_by('-pk')
    paginator = Paginator(photos_list, 1)
    page = request.GET.get('page')
    try:
        photos = paginator.page(page)
    except PageNotAnInteger:
        photos = paginator.page(1)
    except EmptyPage:
        photos = paginator.page(paginator.num_pages)

    context = {
        'user': request.user,
        'site': get_current_site(request),
        'site_name': get_current_site(request).name,
        'photos': photos
    }
    return TemplateResponse(request, 'gallery.html', context)

@staff_member_required
def dashboard_view(request):
    photosessions = Photosession.objects.all().order_by('-datetime')

    media_dict = OrderedDict() # ordered dict, where key is photosession and value is list of medias, sorted in the order of pk ascending
    # e.g. {photosession1: [media1, media2, media3]}

    for photosession in photosessions:
        medias = Media.objects.filter(photosession=photosession).order_by('id')
        media_dict[photosession] = medias

    context = {'photosessions': media_dict}

    return TemplateResponse(request, 'dashboard.html', context)

### REST api views ###

@csrf_exempt
@api_view(['GET'])
def api_root_view(request, format=None):
    return Response({
        'users': reverse("api_users", kwargs={}),
        'location': reverse("api_locations", kwargs={}),
        'campaign': reverse("api_campaigns", kwargs={}),
        'devices': reverse("api_devices", kwargs={}),
        'mediacover_template': reverse("api_mediacover_templates", kwargs={}),
        'mediacover_template_file': reverse("api_mediacover_template_files", kwargs={}),
        'user_site_template': reverse("api_user_site_templates", kwargs={}),
        'user_site_template_file': reverse("api_user_site_template_files", kwargs={}),
        'medias': reverse("api_medias", kwargs={}),
        'photosessions': reverse("api_photosessions", kwargs={})
    })

# stolen from: http://stackoverflow.com/questions/20303252/django-rest-framework-imagefield

class ApiUsersView(APIView):

    permission_classes = (permissions.IsAuthenticatedOrReadOnly,
                          IsSuperuser)

    def get(self, request, format=None):
        users = User.objects.all()
        serializer = UserSerializer(users, many=True, context={'request': request})
        return Response(serializer.data)

class ApiUserView(APIView):

    permission_classes = (permissions.IsAuthenticatedOrReadOnly,
                          IsSuperuser)

    def get_object(self, telephone):
        try:
            user = User.objects.get(telephone=telephone)
            return user
        except User.DoesNotExist:
            raise Http404

    def get(self, request, telephone, format=None):
        user = self.get_object(telephone)
        serializer = UserSerializer(user, context={'request': request})
        return Response(serializer.data)


class ApiLocationsView(APIView):

    permission_classes = (permissions.IsAuthenticatedOrReadOnly,
                          IsSuperuser)

    def get(self, request, format=None):
        locations = Location.objects.all()
        serializer = LocationSerializer(locations, many=True, context={'request': request})
        return Response(serializer.data)

class ApiLocationView(APIView):

    permission_classes = (permissions.IsAuthenticatedOrReadOnly,
                          IsSuperuser)

    def get_object(self, pk):
        try:
            location = Location.objects.get(pk=pk)
            return location
        except Location.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        location = self.get_object(pk)
        serializer = LocationSerializer(location, context={'request': request})
        return Response(serializer.data)


class ApiCampaignsView(APIView):

    permission_classes = (permissions.IsAuthenticatedOrReadOnly,
                          IsSuperuser)

    def get(self, request, format=None):
        campaigns = Campaign.objects.all()
        serializer = CampaignSerializer(campaigns, many=True, context={'request': request})
        return Response(serializer.data)

class ApiCampaignView(APIView):

    permission_classes = (IsSuperuser, )

    def get_object(self, pk):
        try:
            campaign = Campaign.objects.get(pk=pk)
            return campaign
        except Campaign.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        campaign = self.get_object(pk)
        serializer = CampaignSerializer(campaign, context={'request': request})
        return Response(serializer.data)


class ApiDevicesView(APIView):

    permission_classes = (permissions.IsAuthenticatedOrReadOnly,
                          IsSuperuser)

    def get(self, request, format=None):
        devices = Device.objects.all()
        serializer = DeviceSerializer(devices, many=True, context={'request': request})
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = DeviceSerializer(data=request.data, context={'request': request})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class ApiDeviceView(APIView):

    permission_classes = (permissions.IsAuthenticatedOrReadOnly,
                          IsSuperuser)

    def get_object(self, pk):
        try:
            device = Device.objects.get(pk=pk)
            return device
        except Device.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        device = self.get_object(pk)
        serializer = DeviceSerializer(device, context={'request': request})
        return Response(serializer.data)


class ApiMediacoverTemplatesView(APIView):

    permission_classes = (permissions.IsAuthenticatedOrReadOnly,
                          IsSuperuser)

    def get(self, request, format=None):
        mediacover_templates = MediacoverTemplate.objects.all()
        serializer = MediacoverTemplateSerializer(mediacover_templates, many=True, context={'request': request})
        return Response(serializer.data)

class ApiMediacoverTemplateView(APIView):

    permission_classes = (permissions.IsAuthenticatedOrReadOnly,
                          IsSuperuser)

    def get_object(self, pk):
        try:
            mediacover_template = MediacoverTemplate.objects.get(pk=pk)
            return mediacover_template
        except MediacoverTemplate.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        mediacover_template = self.get_object(pk)
        serializer = MediacoverTemplateSerializer(mediacover_template, context={'request': request})
        return Response(serializer.data)


class ApiMediacoverTemplateFilesView(APIView):

    permission_classes = (permissions.IsAuthenticatedOrReadOnly,
                          IsSuperuser)

    def get(self, request, format=None):
        mediacover_template_files = MediacoverTemplateFile.objects.all()
        serializer = MediacoverTemplateFileSerializer(mediacover_template_files, many=True, context={'request': request})
        return Response(serializer.data)

class ApiMediacoverTemplateFileView(APIView):

    permission_classes = (permissions.IsAuthenticatedOrReadOnly,
                          IsSuperuser)

    def get_object(self, pk):
        try:
            mediacover_template_file = MediacoverTemplateFile.objects.get(pk=pk)
            return mediacover_template_file
        except MediacoverTemplateFile.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        mediacover_template_files = self.get_object(pk)
        serializer = MediacoverTemplateFileSerializer(mediacover_template_files, context={'request': request})
        return Response(serializer.data)


class ApiUserSiteTemplatesView(APIView):

    permission_classes = (permissions.IsAuthenticatedOrReadOnly,
                          IsSuperuser)

    def get(self, request, format=None):
        user_site_templates = UserSiteTemplate.objects.all()
        serializer = UserSiteTemplateSerializer(user_site_templates, many=True, context={'request': request})
        return Response(serializer.data)

class ApiUserSiteTemplateView(APIView):

    permission_classes = (permissions.IsAuthenticatedOrReadOnly,
                          IsSuperuser)

    def get_object(self, pk):
        try:
            user_site_template = UserSiteTemplate.objects.get(pk=pk)
            return user_site_template
        except UserSiteTemplate.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        user_site_template = self.get_object(pk)
        serializer = UserSiteTemplateSerializer(user_site_template, context={'request': request})
        return Response(serializer.data)


class ApiUserSiteTemplateFilesView(APIView):

    permission_classes = (permissions.IsAuthenticatedOrReadOnly,
                          IsSuperuser)

    def get(self, request, format=None):
        user_site_template_files = UserSiteTemplateFile.objects.all()
        serializer = UserSiteTemplateFileSerializer(user_site_template_files, many=True, context={'request': request})
        return Response(serializer.data)

class ApiUserSiteTemplateFileView(APIView):

    permission_classes = (permissions.IsAuthenticatedOrReadOnly,
                          IsSuperuser)

    def get_object(self, pk):
        try:
            user_site_template_file = UserSiteTemplateFile.objects.get(pk=pk)
            return user_site_template_file
        except UserSiteTemplate.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        user_site_template_file = self.get_object(pk)
        serializer = UserSiteTemplateFileSerializer(user_site_template_file, context={'request': request})
        return Response(serializer.data)


class ApiMediasView(APIView):

    permission_classes = (permissions.IsAuthenticatedOrReadOnly,
                          IsMediaLocationOwner)

    def get(self, request, format=None):
        medias = Media.objects.all()
        serializer = MediaSerializer(medias, many=True, context={'request': request})
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = MediaSerializer(data=request.data, context={'request': request}) # in case this doesn't work, try: data = JSONParser().parse(request)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ApiMediaView(APIView):

    permission_classes = (permissions.IsAuthenticatedOrReadOnly, 
                          IsMediaLocationOwner)

    def get_object(self, pk):
        try:
            media = Media.objects.get(pk=pk)
            return media
        except Media.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        media = self.get_object(pk)
        serializer = MediaSerializer(media, context={'request': request})
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        media = self.get_object(pk)
        serializer = MediaSerializer(media, data=request.data, context={'request': request})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        media = self.get_object(pk)
        media.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

#    def pre_save(self, obj):
#        obj.user = self.request.user


class ApiPhotosessionViewSet(viewsets.ViewSet):

    permission_classes = (permissions.IsAuthenticatedOrReadOnly,
                          IsPhotosessionLocationOwner)

    def get_serializer_class(self):
        if self.action == 'list' or self.action == 'retrieve' or self.action == 'put' or self.action == 'delete':
            return PhotosessionGetPutSerializer
        else: # create or update
            return PhotosessionPostSerializer

    def get_object(self, pk):
        try:
            photosession = Photosession.objects.get(pk=pk)
            return photosession
        except Photosession.DoesNotExist:
            raise Http404

    def detail(self, request, pk, format=None):
        photosession = self.get_object(pk)
        serializer = self.get_serializer_class()(photosession, context={'request': request})
        return Response(serializer.data)

    def list(self, request, format=None):
        photosessions = Photosession.objects.all()
        serializer = self.get_serializer_class()(photosessions, many=True, context={'request': request})
        return Response(serializer.data)

    def create(self, request, format=None):
        serializer = self.get_serializer_class()(data=request.data, context={'request': request})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        elif 'user' in serializer.errors:
            try:
                user_url = request.data.get('user')
                telephone = user_url.strip('/').split('/')[-1]
                new_user = User.objects.create(telephone=telephone, location=None, is_staff=False, is_superuser=False)
                serializer = self.get_serializer_class()(data=request.data, context={'request': request})
                if serializer.is_valid():
                    serializer.save()
                    return Response(serializer.data, status=status.HTTP_201_CREATED)
                else:
                    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
            except Exception as e:
                print e
                raise Http404
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def update(self, request, pk, format=None):
        photosession = self.get_object(pk)
        serializer = self.get_serializer_class()(photosession, data=request.data, context={'request':request})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, pk, format=None):
        photosession = self.get_object(pk)
        photosession.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)