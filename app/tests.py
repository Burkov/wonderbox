import os

from django.utils import timezone
from django.contrib.auth.hashers import make_password
from django.core.urlresolvers import reverse
from django.core.files import File
from django.conf import settings

from rest_framework.test import APIClient, APITestCase

from app.models import *


def test_get(test_case, new_model_instance, url_list, url_detail):
	'''
	Tests that REST API endpoint correctly replies to GET request.

	Arguments:
		test_case - TestCase instance, where this function is called.
		new_model_instance - model instance, created during setUp(), e.g. User.
		url_list - str, representing url of collection, e.g. '/api/user/'.
		url_detail - str, representing url of a single instance of model, e.g. '/api/user/911';
	'''
	c = APIClient()
	response = c.get(url_list)
	if response.status_code != 200: print response.content
	test_case.assertEqual(response.status_code, 200)
	# print response.content

	response = c.get(url_detail)
	if response.status_code != 200: print response.content
	test_case.assertEqual(response.status_code, 200)

	# test logged-in
	login_status = c.login(telephone="911", password="911")
	test_case.assertTrue(login_status)

	response = c.get(url_list)
	if response.status_code != 200: print response.content
	test_case.assertEqual(response.status_code, 200)

	response = c.get(url_detail)
	if response.status_code != 200: print response.content
	test_case.assertEqual(response.status_code, 200)


def test_no_post_put_delete(test_case, new_model_instance, url_list, url_detail):
	'''
	This function tests that REST API endpoints, given by url_list and url_detail
	can not be modified by POST, PUT or DELETE requests.

	Arguments:
		test_case - TestCase instance, where this function is called.
		new_model_instance - model instance, created during setUp(), e.g. User.
		url_list - str, representing url of collection, e.g. '/api/user/'.
		url_detail - str, representing url of a single instance of model, e.g. '/api/user/911';
	'''
	c = APIClient()

	response = c.post(url_list, {})
	test_case.assertEqual(response.status_code, 403)

	response = c.delete(url_detail)
	test_case.assertEqual(response.status_code, 403)

	response = c.put(url_detail, {})
	test_case.assertEqual(response.status_code, 403)

	# test logged-in
	login_status = c.login(telephone="911", password="911")
	test_case.assertTrue(login_status)

	response = c.post(url_list, {})
	test_case.assertEqual(response.status_code, 405)

	response = c.delete(url_detail)
	test_case.assertEqual(response.status_code, 405)

	response = c.put(url_detail, {})
	test_case.assertEqual(response.status_code, 405)	


class UserTestCase(APITestCase):
	def setUp(self):
		new_admin = User(telephone="911", location=None, is_staff=True, is_superuser=True)
		new_admin.set_password('911')		
		new_admin.save()

		new_location = Location(owner=new_admin, name="GUM")
		new_location.save()

		self.new_user = User(telephone="1234567", password="1234567", location=new_location, is_staff=False, is_superuser=False)
		self.new_user.save()

	def test_get(self):
		url_list = reverse("api_users", kwargs={})
		url_detail = url_list + str(self.new_user.telephone) + '/'

		test_get(self, self.new_user, url_list, url_detail)

	def test_post_put_delete(self):
		url_list = reverse("api_users", kwargs={})
		url_detail = url_list + str(self.new_user.telephone) + '/'

		test_no_post_put_delete(self, self.new_user, url_list, url_detail)

class LocationTestCase(APITestCase):
	def setUp(self):
		new_admin = User(telephone="911", location=None, is_staff=True, is_superuser=True)
		new_admin.set_password('911')
		new_admin.save()

		self.new_location = Location(owner=new_admin, name="GUM")
		self.new_location.save()

	def test_get(self):
		url_list = reverse('api_locations')
		url_detail = url_list + str(self.new_location.id) + '/'

		test_get(self, self.new_location, url_list, url_detail)

	def test_post_put_delete(self):
		url_list = reverse('api_locations')
		url_detail = url_list + str(self.new_location.id) + '/'

		test_no_post_put_delete(self, self.new_location, url_list, url_detail)


class CampaignTestCase(APITestCase):
	def setUp(self):
		new_admin = User(telephone="911", location=None, is_staff=True, is_superuser=True)
		new_admin.set_password('911')
		new_admin.save()

		new_location = Location(owner=new_admin, name="GUM")
		new_location.save()

		self.new_campaign = Campaign(name="Pink ponies", location=new_location, end=timezone.now(), number_of_participants=1)
		self.new_campaign.save()

	def test_get(self):
		url_list = reverse('api_campaigns')
		url_detail = url_list + str(self.new_campaign.id) + '/'

		test_get(self, self.new_campaign, url_list, url_detail)

	def test_post_put_delete(self):
		url_list = reverse('api_locations')
		url_detail = url_list + str(self.new_campaign.id) + '/'

		test_no_post_put_delete(self, self.new_campaign, url_list, url_detail)


class DeviceTestCase(APITestCase):
	def setUp(self):
		new_admin = User(telephone="911", location=None, is_staff=True, is_superuser=True)
		new_admin.set_password('911')
		new_admin.save()

		self.new_location = Location(owner=new_admin, name="GUM")
		self.new_location.save()

		self.new_device = Device(name="GUM1", location=self.new_location, config={}, status={})
		self.new_device.save()

	def test_get(self):
		url_list = reverse('api_devices')
		url_detail = url_list + str(self.new_device.id) + '/'

		test_get(self, self.new_device, url_list, url_detail)

	def test_post(self):
		c = APIClient()

		new_location_url = reverse('api_locations') + str(self.new_location.id) + '/'

		response = c.post(reverse('api_devices'), {'name':'GUM1', 'location':new_location_url, 'config':'{}', 'status':'{}'})
		self.assertEqual(response.status_code, 403)

		login_status = c.login(telephone='911', password='911')
		self.assertTrue(login_status)

		response = c.post(reverse('api_devices'), {'name':'GUM1', 'location':new_location_url, 'config':'{}', 'status':'{}'})
		self.assertEqual(response.status_code, 201)		

	def test_put(self):
		c = APIClient()

		new_location_url = reverse('api_locations') + str(self.new_location.id) + '/'

		response = c.put(reverse('api_devices') + str(self.new_device.id) + '/', {'name':'GUM1', 'location':new_location_url, 'config':'{}', 'status':'{}'})
		self.assertEqual(response.status_code, 403)

		login_status = c.login(telephone='911', password='911')
		self.assertTrue(login_status)

		response = c.put(reverse('api_devices') + str(self.new_device.id) + '/', {'name':'GUM1', 'location':new_location_url, 'config':'{}', 'status':'{}'})
		self.assertEqual(response.status_code, 405)

	def test_delete(self):
		c = APIClient()

		response = c.delete(reverse('api_devices') + str(self.new_device.id) + '/')
		self.assertEqual(response.status_code, 403)

		login_status = c.login(telephone='911', password='911')
		self.assertTrue(login_status)

		response = c.delete(reverse('api_devices') + str(self.new_device.id) + '/')
		self.assertEqual(response.status_code, 405)


class MediacoverTemplateTestCase(APITestCase):
	def setUp(self):
		new_admin = User(telephone="911", location=None, is_staff=True, is_superuser=True)
		new_admin.set_password('911')
		new_admin.save()

		new_location = Location(owner=new_admin, name="GUM")
		new_location.save()

		new_campaign = Campaign(name="Pink ponies", location=new_location, end=timezone.now(), number_of_participants=1)
		new_campaign.save()

		self.new_mediacover_template = MediacoverTemplate(json="")
		self.new_mediacover_template.save()

	def test_get(self):
		url_list = reverse('api_mediacover_templates')
		url_detail = url_list + str(self.new_mediacover_template.id) + "/"

		test_get(self, self.new_mediacover_template, url_list, url_detail)

	def test_post_put_delete(self):
		url_list = reverse('api_mediacover_templates')
		url_detail = url_list + str(self.new_mediacover_template.id) + "/"

		test_no_post_put_delete(self, self.new_mediacover_template, url_list, url_detail)


class MediacoverTemplateFileTestCase(APITestCase):
	def setUp(self):
		new_admin = User(telephone="911", location=None, is_staff=True, is_superuser=True)
		new_admin.set_password('911')
		new_admin.save()

		new_location = Location(owner=new_admin, name="GUM")
		new_location.save()

		new_campaign = Campaign(name="Pink ponies", location=new_location, end=timezone.now(), number_of_participants=1)
		new_campaign.save()

		self.new_mediacover_template = MediacoverTemplate(json="")
		self.new_mediacover_template.save()

		self.new_mediacover_template_file = MediacoverTemplateFile(mediacover_template=self.new_mediacover_template)
		self.new_mediacover_template_file.save()
		self.new_mediacover_template_file.file.save('tests.py', File(open(os.path.join(settings.BASE_DIR, 'app', 'tests.py'))))

	def test_get(self):
		url_list = reverse('api_mediacover_template_files')
		url_detail = url_list + str(self.new_mediacover_template_file.id) + "/"

		test_get(self, self.new_mediacover_template_file, url_list, url_detail)

	def test_post_put_delete(self):
		url_list = reverse('api_mediacover_template_files')
		url_detail = url_list + str(self.new_mediacover_template_file.id) + "/"

		test_no_post_put_delete(self, self.new_mediacover_template_file, url_list, url_detail)


class UserSiteTemplateTestCase(APITestCase):
	def setUp(self):
		new_admin = User(telephone="911", location=None, is_staff=True, is_superuser=True)
		new_admin.set_password('911')		
		new_admin.save()

		new_location = Location(owner=new_admin, name="GUM")
		new_location.save()

		new_campaign = Campaign(name="Pink ponies", location=new_location, end=timezone.now(), number_of_participants=1)
		new_campaign.save()
		
		self.new_user_site_template = UserSiteTemplate(css="", campaign=new_campaign)
		self.new_user_site_template.save()

	def test_get(self):
		url_list = reverse('api_user_site_templates')
		url_detail = url_list + str(self.new_user_site_template.id) + '/'

		test_get(self, self.new_user_site_template, url_list, url_detail)

	def test_post_put_delete(self):
		url_list = reverse('api_user_site_templates')
		url_detail = url_list + str(self.new_user_site_template.id) + '/'

		test_no_post_put_delete(self, self.new_user_site_template, url_list, url_detail)


class UserSiteTemplateFileTestCase(APITestCase):
	def setUp(self):
		new_admin = User(telephone="911", location=None, is_staff=True, is_superuser=True)
		new_admin.set_password('911')		
		new_admin.save()

		new_location = Location(owner=new_admin, name="GUM")
		new_location.save()

		new_campaign = Campaign(name="Pink ponies", location=new_location, end=timezone.now(), number_of_participants=1)
		new_campaign.save()
		
		self.new_user_site_template = UserSiteTemplate(css="", campaign=new_campaign)
		self.new_user_site_template.save()

		self.new_user_site_template_file = UserSiteTemplateFile(user_site_template=self.new_user_site_template)
		self.new_user_site_template_file.save()
		self.new_user_site_template_file.file.save('tests.py', File(open(os.path.join(settings.BASE_DIR, 'app', 'tests.py'))))

	def test_get(self):
		url_list = reverse('api_user_site_template_files')
		url_detail = url_list + str(self.new_user_site_template_file.id) + '/'

		test_get(self, self.new_user_site_template_file, url_list, url_detail)

	def test_post_put_delete(self):
		url_list = reverse('api_user_site_template_files')
		url_detail = url_list + str(self.new_user_site_template_file.id) + '/'

		test_no_post_put_delete(self, self.new_user_site_template_file, url_list, url_detail)


class MediaTestCase(APITestCase):
	def setUp(self):
		self.new_admin = User.objects.create(telephone="911", location=None, is_staff=True, is_superuser=True)
		self.new_admin.set_password('911')
		self.new_admin.save()

		self.new_location = Location(owner=self.new_admin, name="GUM")
		self.new_location.save()

		self.new_device = Device(name="GUM1", location=self.new_location, config={}, status={})
		self.new_device.save()

		self.new_photosession = Photosession(user=self.new_admin, device=self.new_device, sms_status="delivered", params={}, datetime=timezone.now(), output={})
		self.new_photosession.save()

		self.new_media = Media(photosession=self.new_photosession, metadata='{}')
		self.new_media.save()
		self.new_media.file.save('tests.py', File(open(os.path.join(settings.BASE_DIR, 'app', 'tests.py'))))

	def test_get(self):
		url_list = reverse('api_medias')
		url_detail = url_list + str(self.new_media.id) + '/'

		test_get(self, self.new_media, url_list, url_detail)

	def test_post(self):
		c = APIClient()

		new_photosession_url = reverse('api_photosessions') + str(self.new_photosession.id) + '/'

		response = c.post(reverse('api_medias'), {'photosession':new_photosession_url, 'metadata':'{}', 'file':open(os.path.join(settings.BASE_DIR, 'wonderbox', 'static', 'img', 'logo.png'))})
		self.assertEqual(response.status_code, 403)

		login_status = c.login(telephone='911', password='911')
		self.assertTrue(login_status)

		response = c.post(reverse('api_medias'), {'photosession':new_photosession_url, 'metadata':'{}', 'file':open(os.path.join(settings.BASE_DIR, 'wonderbox', 'static', 'img', 'logo.png'))})
		self.assertEqual(response.status_code, 201)

	def test_put(self):
		c = APIClient()

		new_photosession_url = reverse('api_photosessions') + str(self.new_photosession.id) + '/'

		response = c.put(reverse('api_medias') + str(self.new_media.id) + '/', {'photosession':new_photosession_url, 'metadata':'{}', 'file':open(os.path.join(settings.BASE_DIR, 'wonderbox', 'static', 'img', 'logo.png'))})
		self.assertEqual(response.status_code, 403)

		login_status = c.login(telephone='911', password='911')
		self.assertTrue(login_status)

		response = c.put(reverse('api_medias') + str(self.new_media.id) + '/', {'photosession':new_photosession_url, 'metadata':'{}', 'file':open(os.path.join(settings.BASE_DIR, 'wonderbox', 'static', 'img', 'logo.png'))})
		self.assertEqual(response.status_code, 200)

	def test_delete(self):
		c = APIClient()

		response = c.delete(reverse('api_medias') + str(self.new_media.id) + '/')
		self.assertEqual(response.status_code, 403)

		login_status = c.login(telephone='911', password='911')
		self.assertTrue(login_status)

		response = c.delete(reverse('api_medias') + str(self.new_media.id) + '/')
		self.assertEqual(response.status_code, 204)


class PhotosessionTestCase(APITestCase):
	def setUp(self):
		self.new_admin = User.objects.create(telephone="911", location=None, is_staff=True, is_superuser=True)
		self.new_admin.set_password('911')
		self.new_admin.save()

		self.new_location = Location(owner=self.new_admin, name="GUM")
		self.new_location.save()

		self.new_device = Device(name="GUM1", location=self.new_location, config={}, status={})
		self.new_device.save()

		self.new_photosession = Photosession(user=self.new_admin, device=self.new_device, sms_status="delivered", params={}, datetime=timezone.now(), output={})
		self.new_photosession.save()

	def test_get(self):
		url_list = reverse('api_photosessions')
		url_detail = url_list + str(self.new_photosession.id) + '/'

		test_get(self, self.new_photosession, url_list, url_detail)

	def test_post(self):
		c = APIClient()

		new_admin_url = reverse('api_users') + str(self.new_admin.telephone) + '/'
		new_device_url = reverse('api_devices') + str(self.new_device.id) + '/'

		response = c.post(reverse('api_photosessions'), {'user':new_admin_url, 'device':new_device_url, 'sms_status':"delivered", 'params':'{}', 'datetime':timezone.now(), 'output':'{}'})
		self.assertEqual(response.status_code, 403)

		login_status = c.login(telephone='911', password='911')
		self.assertTrue(login_status)

		response = c.post(reverse('api_photosessions'), {'user':new_admin_url, 'device':new_device_url, 'sms_status':"delivered", 'params':'{}', 'datetime':timezone.now(), 'output':'{}'})
		self.assertEqual(response.status_code, 201)

	def test_post_and_create_user(self):
		c = APIClient()

		new_user_url = reverse('api_users') + str('12345') + '/'
		new_device_url = reverse('api_devices') + str(self.new_device.id) + '/'

		response = c.post(reverse('api_photosessions'), {'user':new_user_url, 'device':new_device_url, 'sms_status':"delivered", 'scheduled':True, 'params':'{}', 'datetime':timezone.now(), 'output':'{}'})
		self.assertEqual(response.status_code, 403)

		login_status = c.login(telephone='911', password='911')
		self.assertTrue(login_status)

		response = c.post(reverse('api_photosessions'), {'user':new_user_url, 'device':new_device_url, 'sms_status':"delivered", 'scheduled':True, 'params':'{}', 'datetime':timezone.now(), 'output':'{}'})
		self.assertEqual(response.status_code, 201)

	def test_put(self):
		c = APIClient()

		new_admin_url = reverse('api_users') + str(self.new_admin.telephone) + '/'
		new_device_url = reverse('api_devices') + str(self.new_device.id) + '/'

		response = c.put(reverse('api_photosessions') + str(self.new_photosession.id) + '/', {'user': new_admin_url, 'device':new_device_url, 'sms_status':"delivered", 'scheduled':True, 'params':'{}', 'datetime':timezone.now(), 'output':'{}'}, format="json")
		self.assertEqual(response.status_code, 403)

		login_status = c.login(telephone='911', password='911')
		self.assertTrue(login_status)

		response = c.put(reverse('api_photosessions') + str(self.new_photosession.id) + '/', {'user': new_admin_url, 'device':new_device_url, 'sms_status':"delivered", 'scheduled':True, 'params':'{}', 'datetime':timezone.now(), 'output':'{}'}, format="json")
		self.assertEqual(response.status_code, 200)

	def test_delete(self):
		c = APIClient()

		response = c.delete(reverse('api_photosessions') + str(self.new_photosession.id) + '/')
		self.assertEqual(response.status_code, 403)

		login_status = c.login(telephone='911', password='911')
		self.assertTrue(login_status)

		response = c.delete(reverse('api_photosessions') + str(self.new_photosession.id) + '/')
		self.assertEqual(response.status_code, 204)

