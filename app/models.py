import uuid

from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from django.utils.safestring import mark_safe
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager, Permission
from django.core.urlresolvers import reverse

from django.dispatch import receiver
from django.db.models.signals import post_save

import jsonfield

from tasks import wait

# Custom user model code is stolen from:
# https://www.caktusgroup.com/blog/2013/08/07/migrating-custom-user-model-django/

class UserManager(BaseUserManager):
    def create_user(self, telephone, location, password=None):
        if not telephone:
            raise ValueError('Users must have a telephone number')

        now = timezone.now()
        user = self.model(telephone=telephone, location=location, date_joined=now, last_login=now) # last_login is present in AbstractBaseUser
        user.set_password(password)

        user.save(using=self._db)

        #permission = Permission.objects.get(codename='change_photosession')
        #user.user_permissions.add(permission)
        #
        #permission = Permission.objects.get(codename='change_media')
        #user.user_permissions.add(permission)

        user.save()

        return user

    def create_superuser(self, telephone, password):
        now = timezone.now()
        user = self.create_user(telephone=telephone, location=None, password=password)
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user

class User(AbstractBaseUser, PermissionsMixin):
    telephone = models.CharField(_('telephone number'),max_length=255, unique=True)
    location = models.ForeignKey('Location', related_name="user", null=True, blank=True)
    is_active = models.BooleanField(
        _('active'),
        default=True,
        help_text=_('Designates whether this user should be treated as active. Unselect this instead of deleting accounts.')
    )
    is_staff = models.BooleanField(
        _('staff status'), 
        default=False,
        help_text=_('Designates whether the user can log into this admin site.')
    )
    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)

    objects = UserManager()

    USERNAME_FIELD = 'telephone'
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def __str__(self):
        return self.telephone

    def get_short_name(self):
        return self.telephone

    def get_full_name(self):
        return self.telephone

#    def has_perm(self, perm, obj=None):
#        '''I override this method to allow staff users edit their photosession and media'''
#        print perm
#        if perm =='app.change_photosession' or perm == 'app.change_media':
#            if self.is_staff == True:
#                print "allowed to change photosession or media"
#                return True
#
#        return super(User, self).has_perm(perm, obj)

def default_location_name():
    '''Creates a random uuid to name this location'''
    return "location %s" % uuid.uuid4()

class Location(models.Model):
    name = models.CharField(max_length=255, default=default_location_name)
    owner = models.ForeignKey('User', related_name="owner", default=1)
    parent = models.ForeignKey('Location', null=True, blank=True, default=None)

    def __unicode__(self):
        return self.name

def default_campaign_name():
    '''Creates a random uuid to name this campaign'''
    return "Campaign %s" % uuid.uuid4()

class Campaign(models.Model):
    name = models.CharField(max_length=255, default=default_campaign_name)
    location = models.ForeignKey(Location, default=1)
    end = models.DateTimeField()
    number_of_participants = models.IntegerField(default=1)

    def __unicode__(self):
        return self.name


def default_device_name():
    '''Creates a random uuid to name this device'''
    return "Device %s" % uuid.uuid4()

class Device(models.Model):
    name = models.CharField(max_length=255, default=default_device_name)
    location = models.ForeignKey('Location', default=1)
    config = jsonfield.JSONField()
    status = jsonfield.JSONField()

    def __unicode__(self):
        return self.name


def upload_mediacover_json_to(instance, filename):
    path_elements = [
        'Templates', 
        'Mediacovers', 
        str(instance.uuid),
        'json.json'
    ]
    return '/'.join(path_elements)

class MediacoverTemplate(models.Model):
    uuid = models.CharField(max_length=255, default=uuid.uuid4)
    description = models.CharField(max_length=255, default="")
    campaign = models.ForeignKey(Campaign, default=1)
    json = models.FileField(upload_to=upload_mediacover_json_to, max_length=255)

    def __unicode__(self):
        return "MediacoverTemplate %s" % self.uuid



def upload_mediacover_template_file_to(instance, filename):
    path_elements = [
        'Templates',
        'Mediacovers',
        str(instance.mediacover_template.uuid),
        str(instance.uuid)
    ]
    return '/'.join(path_elements)

class MediacoverTemplateFile(models.Model):
    uuid = models.CharField(max_length=255, default=uuid.uuid4)
    mediacover_template = models.ForeignKey(MediacoverTemplate, default=1)
    file = models.FileField(upload_to=upload_mediacover_template_file_to, max_length=255)

def upload_user_site_template_css_to(instance, filename):
    path_elements = [
        'Templates',
        'Usersites',
        str(instance.uuid),
        'css.css'
    ]
    return '/'.join(path_elements)

class UserSiteTemplate(models.Model):
    uuid = models.CharField(max_length=255, default=uuid.uuid4)
    css = models.FileField(upload_to=upload_user_site_template_css_to)
    campaign = models.ForeignKey(Campaign, default=1)


def upload_user_site_template_file_to(instance, filename):
    path_elements = [
        'Templates',
        'Usersites',
        str(instance.user_site_template.uuid),
        str(instance.uuid)
    ]
    return '/'.join(path_elements)

class UserSiteTemplateFile(models.Model):
    uuid = models.CharField(max_length=255, default=uuid.uuid4)
    user_site_template = models.ForeignKey('UserSiteTemplate', default=1)
    file = models.FileField(upload_to=upload_user_site_template_file_to, max_length=255)



# stolen from:
# http://stackoverflow.com/questions/10249517/how-to-django-s-inlinemodeladmin-tabularinline-or-stackedinline-show-thumbnail
def upload_media_to(instance, filename):
    path_elements = [
        instance.photosession.user.telephone,
        unicode(instance.photosession.datetime.date()),
        filename
    ]
    return '/'.join(path_elements)

class Media(models.Model):
    file = models.ImageField(upload_to=upload_media_to, max_length=255)
    photosession = models.ForeignKey('Photosession', default=1)
    metadata = jsonfield.JSONField()
    public = models.BooleanField(default=False)

    def image_tag(self):
        return mark_safe(u'<img src="%s" width=100/>' % (self.file.url))
    image_tag.short_description = 'Thumbnail'    
    image_tag.allow_tags = True


class Photosession(models.Model):
    user = models.ForeignKey(User, default=1)
    device = models.ForeignKey('Device', default=1)
    sms_status = models.CharField(max_length=255, null=True, blank=True)
    scheduled = models.BooleanField(default=False)
    params = jsonfield.JSONField()
    datetime = models.DateTimeField(default=timezone.now)
    output = jsonfield.JSONField()

    def __unicode__(self):
        return "Photosession object: telephone = %s, datetime = %s, pk = %s" % (self.user.telephone, self.datetime, self.pk)

@receiver(post_save, sender=Photosession)
def run_wait(**kwargs):
    # see kwargs keys in Django reference for signals
    instance_pk = kwargs['instance'].pk
    # here we can access the database and retreive required models for task
    if kwargs['instance'].scheduled:
        task_id = wait.delay(1, instance_pk).id
