from django.utils import timezone

from rest_framework import serializers

from app.models import User, Location, Campaign, Device, MediacoverTemplate, MediacoverTemplateFile, UserSiteTemplate, UserSiteTemplateFile, Media, Photosession


class JSONSerializerField(serializers.Field):
    """ Serializer for JSONField -- required to make field writable"""
    def to_internal_value(self, data):
        return data
    def to_representation(self, value):
        return value



class UserSerializer(serializers.HyperlinkedModelSerializer):
#    photos = serializers.HyperlinkedRelatedField(
#        source="photo",
#        many=True,
#        read_only=True,
#        view_name='api_photo'
#    )
    location = serializers.HyperlinkedRelatedField(read_only=False, queryset=Location.objects.all(), view_name='api_location')

    class Meta:
        model = User
        #fields = ('telephone', 'photo')
        fields = ('telephone', 'location', 'is_active', 'is_staff', 'date_joined')

class LocationSerializer(serializers.HyperlinkedModelSerializer):
    owner = serializers.HyperlinkedRelatedField(read_only=False, queryset=User.objects.all(), view_name='api_user', lookup_field='telephone')
    parent = serializers.HyperlinkedRelatedField(read_only=False, queryset=Location.objects.all(), view_name='api_location')

    class Meta:
        model = Location
        fields = ('name', 'owner', 'parent')


class CampaignSerializer(serializers.HyperlinkedModelSerializer):
    location = serializers.HyperlinkedRelatedField(read_only=False, queryset=Location.objects.all(), view_name='api_location')

    class Meta:
        model = Campaign
        fields = ('name', 'location', 'end', 'number_of_participants')

class DeviceSerializer(serializers.Serializer):
    name = serializers.CharField()
    location = serializers.HyperlinkedRelatedField(read_only=True, view_name='api_location')
    config = JSONSerializerField()
    status = JSONSerializerField()

    def create(self, validated_data):
        return Device.objects.create(**validated_data)

    def update(self, instance, validated_data):
        device_pk = self.validated_data['pk']
        device = Device.objects.get(pk=device_pk)
        device.save()
        return device

#        photos = Photo.objects.filter(device=device)

#    photos = serializers.HyperlinkedRelatedField(
#        source="photo",
#        many=True,
#        queryset=Photo.objects.filter(device__pk=self.kwargs['pk']),
#        view_name='api_photo'
#    )
#
    class Meta:
        model = Device


class MediacoverTemplateSerializer(serializers.HyperlinkedModelSerializer):
    uuid = serializers.CharField()
    description = serializers.CharField()
    campaign = serializers.HyperlinkedRelatedField(queryset=Campaign.objects.all(), read_only=False, view_name='api_campaign')
    json = serializers.FileField()

    class Meta:
        model = MediacoverTemplate
        fields = ('uuid', 'description', 'campaign', 'json')


class MediacoverTemplateFileSerializer(serializers.HyperlinkedModelSerializer):
    uuid = serializers.CharField()
    mediacover_template = serializers.HyperlinkedRelatedField(queryset=MediacoverTemplate.objects.all(), read_only=False, view_name="api_mediacover_template")
    file = serializers.FileField()

    class Meta:
        model = MediacoverTemplateFile
        fields = ('uuid', 'mediacover_template', 'file')


class UserSiteTemplateSerializer(serializers.Serializer):
    uuid = serializers.CharField()
    css = serializers.FileField()
    campaign = serializers.HyperlinkedRelatedField(queryset=Campaign.objects.all(), read_only=False, view_name='api_campaign')

    class Meta:
        model = UserSiteTemplate
        fields = ('uuid', 'css', 'campaign')


class UserSiteTemplateFileSerializer(serializers.Serializer):
    uuid = serializers.CharField()
    user_site_template = serializers.HyperlinkedRelatedField(queryset=UserSiteTemplate.objects.all(), read_only=False, view_name='api_user_site_template')
    file = serializers.FileField()

    class Meta:
        model = UserSiteTemplateFile
        fields = ('uuid', 'user_site_template', 'file')

class MediaSerializer(serializers.HyperlinkedModelSerializer):
    file = serializers.ImageField()    
    photosession = serializers.HyperlinkedRelatedField(queryset=Photosession.objects.all(), read_only=False, view_name='api_photosession')
    metadata = JSONSerializerField()
    public = serializers.BooleanField()

    def create(self, validated_data):
        file = self.validated_data['file']
        photosession = self.validated_data['photosession']
        metadata = self.validated_data['metadata']
        public = self.validated_data['public']

        output = Media.objects.create(file=file, photosession=photosession, metadata=metadata, public=public)
        return output
       

    def update(self, instance, validated_data):
        file = self.validated_data['file']
        photosession = self.validated_data['photosession']
        metadata = self.validated_data['metadata']
        public = self.validated_data['public']

        instance.file = file
        instance.photosession = photosession
        instance.metadata = metadata

        instance.save()

        return instance

    class Meta:
        model = Media
        fields = ('file', 'photosession', 'metadata', 'public')


class PhotosessionGetPutSerializer(serializers.HyperlinkedModelSerializer):
    '''This serializer is used for 'list' and 'retreive' methods.
    '''    
    user = serializers.HyperlinkedRelatedField(read_only=False, queryset=User.objects.all(), view_name='api_user', lookup_field='telephone')
    device = serializers.HyperlinkedRelatedField(read_only=False, queryset=Device.objects.all(), view_name='api_device')
    sms_status = serializers.CharField()
    scheduled = serializers.BooleanField()
    params = JSONSerializerField()
    datetime = serializers.DateTimeField()
    output = JSONSerializerField()

    class Meta:
        model = Photosession
        fields = ('user', 'device', 'sms_status', 'scheduled', 'params', 'datetime', 'output')

    def update(self, instance, validated_data):
        user = self.validated_data['user']
        device = self.validated_data['device']
        sms_status = self.validated_data['sms_status']
        scheduled = self.validated_data['scheduled']
        params = self.validated_data['params']
        datetime = timezone.now()
        output = self.validated_data['output']

        instance.user = user
        instance.device = device
        instance.sms_status = sms_status
        instance.params = params
        instance.datetime = datetime
        instance.output = output

        instance.save()
        return instance


class PhotosessionPostSerializer(serializers.HyperlinkedModelSerializer):
    '''This serializer is used for 'create' and 'update' methods;
    it omits several fields that should be populated by django itself.
    '''
    user = serializers.HyperlinkedRelatedField(read_only=False, queryset=User.objects.all(), view_name='api_user', lookup_field='telephone')
    device = serializers.HyperlinkedRelatedField(read_only=False, queryset=Device.objects.all(), view_name='api_device')
    sms_status = serializers.CharField()
    scheduled = serializers.BooleanField()
    params = JSONSerializerField()
    output = JSONSerializerField()

    class Meta:
        model = Photosession
        fields = ('user', 'device', 'sms_status', 'scheduled', 'params', 'output')

    def create(self, validated_data):
        user = self.validated_data['user']
        device = self.validated_data['device']
        sms_status = self.validated_data['sms_status']
        scheduled = self.validated_data['scheduled']
        params = self.validated_data['params']
        datetime = timezone.now()
        output = self.validated_data['output']

        result = Photosession.objects.create(user=user, device=device, sms_status=sms_status, scheduled=scheduled, params=params, datetime=datetime, output=output)
        return result


#class PhotosessionSerializer(serializers.HyperlinkedModelSerializer):
#    user = serializers.HyperlinkedRelatedField(read_only=False, queryset=User.objects.all(), view_name='api_user', lookup_field='telephone')
#    device = serializers.HyperlinkedRelatedField(read_only=False, queryset=Device.objects.all(), view_name='api_device')
#    sms_status = serializers.CharField()
#    params = JSONSerializerField()
#    datetime = serializers.DateTimeField()
#    output = JSONSerializerField()
#
#    def create(self, validated_data):
#        user = self.validated_data['user']
#        device = self.validated_data['device']
#        sms_status = self.validated_data['sms_status']
#        params = self.validated_data['params']
#        datetime = self.validated_data['datetime']
#        output = self.validated_data['output']
#
#        result = Photosession.objects.create(user=user, device=device, sms_status=sms_status, params=params, datetime=datetime, output=output)
#        return result
#
#    def update(self, instance, validated_data):
#        user = self.validated_data['user']
#        device = self.validated_data['device']
#        sms_status = self.validated_data['sms_status']
#        params = self.validated_data['params']
#        datetime = self.validated_data['datetime']
#        output = self.validated_data['output']
#
#        instance.user = user
#        instance.device = device
#        instance.sms_status = sms_status
#        instance.params = params
#        instance.datetime = datetime
#        instance.output = output
#
#        instance.save()
#        return instance
#
#    class Meta:
#        model = Photosession
#        fields = ('user', 'device', 'sms_status', 'params', 'datetime', 'output')
