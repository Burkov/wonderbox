How to deploy this project:
===========================

Required packages:
------------------

These packages should be installed on your system in order to deploy Wonderbox.
I assume that your Operating System is Debian 8. If it's not, than you'd
better install the appropriate versions of packages, listed here, with pip.

### Deb-packages:
 * python2.7 (automatically installed)
 * django 1.7.x
 * nginx 1.6
 * uwsgi 2.0
 * uwsgi-plugin-python
 * postgresql 9.4
 * python-psycopg2 2.5.4
 * python-django-celery 3.1 (automatically includes python-celery)
 * rabbitmq-server 3.3
 * python-django-jsonfield 0.9.13

Those packages should be installed globally with aptitude or apt-get.

### Pip packages:
 * djangorestframework 3.1

Alternatively, jsonfield package can be installed right from bitbucket
 * django-jsonfield 0.9.14 from https://bitbucket.org/schinckel/django-jsonfield/commits/all

pip packages can be installed globally (in system site-packages/dist-packages)
or in virtualenv.

I prefer to donwload fresh versions of python packages straight from github, not from PyPI,
and install them locally (-e flag of pip)

    git clone -b version-3.1 https://github.com/tomchristie/django-rest-framework.git
    pip install -e django-rest-framework

In case of mercurial django-jsonfield repository on bitbucket, say
    hg clone https://Burkov@bitbucket.org/schinckel/django-jsonfield
    pip install -e django-jsonfield

Create rabbitmq user, password and vhost:
-----------------------------------------
For explanation of the following commands, see: https://www.rabbitmq.com/man/rabbitmqctl.1.man.html.

    rabbitmqctl add_user rabbit_login rabbit_password
    rabbitmqctl add_vhost rabbit_vhost
    rabbitmqctl set_permissions -p rabbit_vhost rabbit_login ".*" ".*" ".*"

Important configuration files:
------------------------------

 * /etc/nginx/sites-available/wonderbox.conf - configuration of nginx
 * /etc/nginx/sites-enabled/wonderbox.conf - symlink to previous file
 * /etc/uwsgi/apps-available/wonderbox.ini - configuration of uwsgi
 * /etc/uwsgi/apps-enabled/wonderbox.ini - symlink to previous file
 * /etc/rc.local - this starts uwsgi in emperor mode upon system startup
 * /etc/postgresql/9.4/main/postgresql.conf - here you define hosts/ports, postgres is listening to
 * /etc/postgresql/9.4/main/pg\_hba.conf - here you define the mode of authentication in postgres database, user etc.
 * /etc/init.d/celeryd - this script runs celery daemon (taken from celery github) (use update-rc.d to create symlinks to it for runlevels)
 * /etc/default/celeryd - this is config file with celery settings, made by me and read by /etc/init.d/celeryd


Altenative installation - virtualenv:
-------------------------------------

1) Create a virtualenv with system site packages enabled:

    virtualenv ENV --system-site-packages

2) Activate virtualenv:

    source ENV/bin/activate

3) Download django and djangorestframework from github:

    git clone -b stable/1.7.x https://github.com/django/django.git
    git clone -b version-3.1 https://github.com/tomchristie/django-rest-framework.git

4) Install them into the virtualenv from git directories:

    pip install -e django
    pip install -e django-rest-framework


How to send requests via cURL:
------------------------------
Modification of database models requires authentication. 

To modify data, related to a certain network (Media or Photosession), you need to be the user, who has is\_staff=True and is the owner of that network. 

To modify anything else, you need to be the superuser.

Here is an example of curl request, POSTing a new Media:

    curl -u username:password -F "photosession=http://localhost:8000/api/photosession/1/" -F "metadata={}" -F "file=@/home/burkov/Pictures/WISP.png" localhost:8000/api/media/ | less

POSTing a new Photosession:

    curl -u 89162447184:89162447184 -F "user=http://photopost.me/api/user/89162447184/" -F "device=http://photopost.me/api/device/1/" -F "sms_status=dunno" -F "params={}" -F "datetime=2015-07-31T10:18:39Z" -F "output={}" photopost.me/api/photosession/ | less

This request WON'T WORK (cause it lacks authentication information):

    curl -X POST localhost:8000/api/device/

Database commands:
------------------

To create wonderbox database, say:

    sudo su - postgres -c "createdb -U postgres wonderbox"

To login into that database under postgres user, say:

    sudo su - postgres -c psql

In the command prompt of postgres say:

    \l

to list available databases and check, if wonderbox was created or not.

